This is a series of powershell scripts, useful to all sys admins to save time,
and help automate those common tasks. 

DISABLE USESR SCRIPT

Disables User
Moves to Disabled OU
Changes Description to date disabled
Removes all Security Groups except Domain Users
Hides user from GAL
Disables ActiveSync
Removes all Calendar Meeting items in Exchange
Queries AD for Manager of the User and Assigns mailbox to them.

################################################################

SESSION ENUMERATION HARDENING SCRIPT

During the attack phase, network enumeration is a step to map out the network.
Tools like blood hound and netsess.exe can be used to gain valuable information
from sessions on said network.
when computers update their GPO's, that query session contains valuable info
and can help an attacker map out the environment.
Attackers can find out about servers, sid, users, ip address, permissions levels,

#################################################################

DNS HEALTH CHECK SCRIPT

This script runs a report, using dfsdiag and looping through all DC's,
to check and confirm replication and functionality, out putting a report.

#################################################################

MICROSOFT SQL HEALTH CHECK SCRIPT WIN SERVER 2008 - 2012R2

Fetches Services, DB SIZE, Buffer Ratio and User Details

##################################################################

MICROSOFT SQL ENUMERATION SCRIPT

Script attempts to gather MSSQL information on network, to provide an overview
of all things SQL related, to allow cleanup, health checks and documentation.

##################################################################

JAWS PRIVILEGE ESCALATION VECTORS CHECK SCRIPT

This script checks the surface area, for possible privilege escalation points.
This is very useful for CTF's or for pentesting.
Tactical Malware often checks these surface areas, and as a sys admin, you want
to know where you may be vulnerable, so as to allow proactive counter measures. 

#################################################################

DOMAIN LOCK DOWN

Another script, to evaluate the domain against common types of attacks.
Has different flags to allow for evaluation only, or evaluation and remediation
only after prompting the user for consent.

###################################################################